import { Negociacao } from '../models/Negociacao';
import { NegociacoesView } from '../views/NegociacoesView';
import { Negociacoes } from '../models/Negociacoes';
import { Mensagens } from '../views/Mensagens';

export class NegociacaoController {
	private _inputData: HTMLInputElement;
	private _inputQuantidade: HTMLInputElement;
	private _inputValor: HTMLInputElement;
	private _negociacoes = new Negociacoes();
	private _negociacoesview = new NegociacoesView('#NegociacoesView');
	private _mensagens = new Mensagens('#mensagem');

	constructor() {
		this._inputData = document.querySelector('#data');
		this._inputQuantidade = document.querySelector('#quantidade');
		this._inputValor = document.querySelector('#valor');
		this._negociacoesview.update(this._negociacoes);
	}

	adiciona(event: Event) {
		event.preventDefault();
		const negociacao = new Negociacao(
			new Date(this._inputData.value.replace(/-/g, '/')),
			parseInt(this._inputQuantidade.value),
			parseFloat(this._inputValor.value),
		);
		console.log(negociacao);

		this._negociacoes.adiciona(negociacao);

		this._negociacoesview.update(this._negociacoes);
		this._mensagens.update('Operação realizada com sucesso!');
	}
}

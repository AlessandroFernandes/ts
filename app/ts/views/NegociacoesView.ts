import { View } from './View';
import { Negociacoes } from '../models/Negociacoes';


export class NegociacoesView extends View<Negociacoes> {
	template(model: Negociacoes): string {
		return `
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th>Data</th>
                        <th>Quantidade</th>
                        <th>Valor</th>
                        <th>Volume</th>
                    </tr>
                </thead>

                <tbody>
                    ${model
						.paraArray()
						.map(
							data =>
								`
                        <tr>
                            <td>${data.data.getDate()}/${data.data.getMonth()}/${data.data.getFullYear()}</td>
                            <td>${data.quantidade}</td>
                            <td>${data.valor}</td>
                            <td>${data.volume}</td>
                        </tr>
                        `,
						)
						.join('')}
                </tbody>

                <tfoot>
                </tfoot>
            </table>
        
        `;
	}
}

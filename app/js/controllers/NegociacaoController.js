System.register(["../models/Negociacao", "../views/NegociacoesView", "../models/Negociacoes", "../views/Mensagens"], function (exports_1, context_1) {
    "use strict";
    var Negociacao_1, NegociacoesView_1, Negociacoes_1, Mensagens_1, NegociacaoController;
    var __moduleName = context_1 && context_1.id;
    return {
        setters: [
            function (Negociacao_1_1) {
                Negociacao_1 = Negociacao_1_1;
            },
            function (NegociacoesView_1_1) {
                NegociacoesView_1 = NegociacoesView_1_1;
            },
            function (Negociacoes_1_1) {
                Negociacoes_1 = Negociacoes_1_1;
            },
            function (Mensagens_1_1) {
                Mensagens_1 = Mensagens_1_1;
            }
        ],
        execute: function () {
            NegociacaoController = class NegociacaoController {
                constructor() {
                    this._negociacoes = new Negociacoes_1.Negociacoes();
                    this._negociacoesview = new NegociacoesView_1.NegociacoesView('#NegociacoesView');
                    this._mensagens = new Mensagens_1.Mensagens('#mensagem');
                    this._inputData = document.querySelector('#data');
                    this._inputQuantidade = document.querySelector('#quantidade');
                    this._inputValor = document.querySelector('#valor');
                    this._negociacoesview.update(this._negociacoes);
                }
                adiciona(event) {
                    event.preventDefault();
                    const negociacao = new Negociacao_1.Negociacao(new Date(this._inputData.value.replace(/-/g, '/')), parseInt(this._inputQuantidade.value), parseFloat(this._inputValor.value));
                    console.log(negociacao);
                    this._negociacoes.adiciona(negociacao);
                    this._negociacoesview.update(this._negociacoes);
                    this._mensagens.update('Operação realizada com sucesso!');
                }
            };
            exports_1("NegociacaoController", NegociacaoController);
        }
    };
});

System.register(["./View"], function (exports_1, context_1) {
    "use strict";
    var View_1, NegociacoesView;
    var __moduleName = context_1 && context_1.id;
    return {
        setters: [
            function (View_1_1) {
                View_1 = View_1_1;
            }
        ],
        execute: function () {
            NegociacoesView = class NegociacoesView extends View_1.View {
                template(model) {
                    return `
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th>Data</th>
                        <th>Quantidade</th>
                        <th>Valor</th>
                        <th>Volume</th>
                    </tr>
                </thead>

                <tbody>
                    ${model
                        .paraArray()
                        .map(data => `
                        <tr>
                            <td>${data.data.getDate()}/${data.data.getMonth()}/${data.data.getFullYear()}</td>
                            <td>${data.quantidade}</td>
                            <td>${data.valor}</td>
                            <td>${data.volume}</td>
                        </tr>
                        `)
                        .join('')}
                </tbody>

                <tfoot>
                </tfoot>
            </table>
        
        `;
                }
            };
            exports_1("NegociacoesView", NegociacoesView);
        }
    };
});
